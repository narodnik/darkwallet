use darkwallet as dw;

async fn start() -> dw::Result<()> {
    Ok(())
}

fn main() {
    dw::smol_auto_run(start());
}
