use futures::io;
use futures::prelude::*;
use log::*;
use simplelog::*;
use smol::{Async, Timer};
use std::collections::HashMap;
use std::net::{SocketAddr, TcpListener, TcpStream};

use std::time::Duration;

use darkwallet as dw;

/*
 * The TITAN service.
 *
 * This is the precursor to our blockchain system. It is a simple centralized database consisting
 * of encrypted blobs that we call slabs.
 *
 * Each slab is encrypted for a single key. This is done through the DH crypto system.
 * Clients download the slab headers, derive their DH key and check whether it matches the ss_hash
 * field. If so then they also download the ciphertext (as well as some fake ones) for decrypting.
 * This method is anonymous and persistant.
 *
 * This centralized service will be replaced by a simple PoS system. The network protocol though
 * is already kind of similar to what we will implement.
 * It is stateless and async.
 */

async fn _echo(mut stream: Async<TcpStream>) -> io::Result<()> {
    Timer::after(Duration::from_secs(5)).await;

    let mut command = [0u8; 1];
    stream.read_exact(&mut command).await?;

    println!("read succeeeded");
    Ok(())
}

type ConnectionsMap = async_dup::Arc<
    async_std::sync::Mutex<HashMap<SocketAddr, async_channel::Sender<dw::net::Message>>>,
>;

async fn start() -> dw::Result<()> {
    let slabman = dw::net::SlabsManager::new();

    // Create a listener.
    let listener = Async::<TcpListener>::bind("127.0.0.1:7445")?;
    info!("Listening on {}", listener.get_ref().local_addr()?);

    let connections = async_dup::Arc::new(async_std::sync::Mutex::new(HashMap::new()));

    loop {
        let (stream, peer_addr) = listener.accept().await?;
        info!("Accepted client: {}", peer_addr);
        let stream = async_dup::Arc::new(stream);

        // Channel (queue) for sending data to the client
        let (send_sx, send_rx) = async_channel::unbounded::<dw::net::Message>();
        connections.lock().await.insert(peer_addr, send_sx.clone());

        let slabman2 = slabman.clone();
        let connections2 = connections.clone();

        smol::Task::spawn(async move {
            match process(
                stream,
                slabman2,
                connections2.clone(),
                (send_sx, send_rx),
                &peer_addr,
            )
            .await
            {
                Ok(()) => {
                    warn!("Peer {} timeout", peer_addr);
                }
                Err(err) => {
                    warn!("Peer {} disconnected: {}", peer_addr, err);
                }
            }
            connections2.lock().await.remove(&peer_addr);
        })
        .detach();
    }
}

async fn process(
    mut stream: dw::net::AsyncTcpStream,
    slabman: dw::net::SlabsManagerSafe,
    _connections: ConnectionsMap,
    (send_sx, send_rx): (
        async_channel::Sender<dw::net::Message>,
        async_channel::Receiver<dw::net::Message>,
    ),
    _self_addr: &SocketAddr,
) -> dw::Result<()> {
    let inactivity_timer = dw::net::InactivityTimer::new();

    loop {
        let event = dw::net::select_event(&mut stream, &send_rx, &inactivity_timer).await?;

        match event {
            dw::net::Event::Send(message) => {
                dw::net::send_message(&mut stream, message).await?;
            }
            dw::net::Event::Receive(message) => {
                inactivity_timer.reset().await?;
                protocol(message, &send_sx, &slabman).await?;
            }
            dw::net::Event::Timeout => break,
        }
    }

    inactivity_timer.stop().await;

    // Connection timed out
    Ok(())
}

async fn protocol(
    message: dw::net::Message,
    send_sx: &async_channel::Sender<dw::net::Message>,
    slabman: &dw::net::SlabsManagerSafe,
) -> dw::Result<()> {
    match message {
        dw::net::Message::Ping => {
            send_sx.send(dw::net::Message::Pong).await?;
        }
        dw::net::Message::Pong => {}
        dw::net::Message::Put(message) => {
            //let message = dw::net::PutMessage::decode(Cursor::new(packet.payload))?;
            let slab = dw::net::Slab {
                ephem_public: message.ephem_public,
                ss_hash: message.ss_hash,
                ciphertext: message.ciphertext,
            };

            let height = {
                let mut slabman = slabman.lock().await;
                slabman.add(slab.clone());
                slabman.last_height()
            };
            debug!("Added new slab at height={}", height);
            // Store in index
            send_sx
                .send(dw::net::Message::Inv(dw::net::InvMessage {
                    height,
                    ephem_public: slab.ephem_public,
                    ss_hash: slab.ss_hash,
                    cipher_hash: slab.cipher_hash(),
                }))
                .await?;
        }
        dw::net::Message::Inv(_message) => {
            // Ignore this message
        }
        dw::net::Message::GetSlabs(message) => {
            // Serve invs
            let slabman = slabman.lock().await;
            if message.start_height == 0 {
                return Err(dw::Error::MalformedPacket);
            }
            let end_height = if slabman.last_height() < message.end_height {
                slabman.last_height()
            } else {
                message.end_height
            };
            // Fetch missing block headers
            for height in message.start_height..=end_height {
                send_sx
                    .send(dw::net::Message::Inv(slabman.inv(height)))
                    .await?;
            }
        }
        dw::net::Message::GetCiphertext(message) => {
            // Serve ciphertext
            let cipher_hash = message.cipher_hash;
            match slabman.lock().await.get_ciphertext(&cipher_hash) {
                Some(ciphertext) => {
                    send_sx
                        .send(dw::net::Message::Ciphertext(dw::net::CiphertextMessage {
                            ciphertext: ciphertext.clone(),
                        }))
                        .await?;
                }
                None => {
                    debug!("Ciphertext not found. Skipping {:x?}", cipher_hash);
                }
            }
        }
        dw::net::Message::Ciphertext(_message) => {
            // Ignore this message
        }
    }

    Ok(())
}

fn main() {
    CombinedLogger::init(vec![
        TermLogger::new(LevelFilter::Debug, Config::default(), TerminalMode::Mixed).unwrap(),
        WriteLogger::new(
            LevelFilter::Debug,
            Config::default(),
            std::fs::File::create("/tmp/dftitan.log").unwrap(),
        ),
    ])
    .unwrap();

    dw::smol_auto_run(start());
}
