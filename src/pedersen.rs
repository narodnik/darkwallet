use bls12_381 as bls;

use crate::bls_extensions::*;
use crate::parameters::*;

pub type PedersenCommit = bls::G1Projective;

pub fn compute_pedersen<R: RngInstance>(
    params: &Parameters<R>,
    blind: &bls::Scalar,
    value: &bls::Scalar,
) -> PedersenCommit {
    assert!(params.hs.len() > 0);
    params.g1 * blind + params.hs[0] * value
}

pub fn compute_pedersen_with_u64<R: RngInstance>(
    params: &Parameters<R>,
    blind: &bls::Scalar,
    value: u64,
) -> PedersenCommit {
    assert!(params.hs.len() > 0);
    let value = bls::Scalar::from(value);
    compute_pedersen(params, blind, &value)
}
